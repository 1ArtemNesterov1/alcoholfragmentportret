package com.example.alcoholic;

import android.os.Parcel;
import android.os.Parcelable;

public class Drink implements Parcelable {
    private String strDrink;

    private String strDrinkThumb;

    private String idDrink;

    public Drink(String strDrink, String strDrinkThumb, String idDrink) {
        this.strDrink = strDrink;
        this.strDrinkThumb = strDrinkThumb;
        this.idDrink = idDrink;
    }

    protected Drink(Parcel in) {
        strDrink = in.readString();
        strDrinkThumb = in.readString();
        idDrink = in.readString();
    }

    public static final Creator<Drink> CREATOR = new Creator<Drink>() {
        @Override
        public Drink createFromParcel(Parcel in) {
            return new Drink(in);
        }

        @Override
        public Drink[] newArray(int size) {
            return new Drink[size];
        }
    };

    public String getStrDrink() {
        return strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strDrink);
        dest.writeString(strDrinkThumb);
        dest.writeString(idDrink);
    }
}
