package com.example.alcoholic;

import java.util.ArrayList;
import java.util.List;

public class Converter {

public static List<Drink> convertRequest(List<RequestModel> requestModel){
    List<Drink> drinks = new ArrayList<>();

    for (int i = 0; i < requestModel.size(); i++) {

        drinks.add(new Drink(
                requestModel.get(i).getStrDrink(),
                requestModel.get(i).getStrDrinkThumb(),
                requestModel.get(i).getIdDrink()));
    }
    return drinks;
}
}