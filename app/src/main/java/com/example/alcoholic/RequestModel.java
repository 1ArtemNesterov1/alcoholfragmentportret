package com.example.alcoholic;

import java.util.List;

public class RequestModel {

    private List<Drink> drinks = null;
    private String strDrink;

    private String strDrinkThumb;

    private String idDrink;

    public RequestModel(List<Drink> drinks, String strDrink, String strDrinkThumb, String idDrink) {
        this.drinks = drinks;
        this.strDrink = strDrink;
        this.strDrinkThumb = strDrinkThumb;
       this.idDrink = idDrink;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public String getStrDrink() {
        return strDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }
}



