package com.example.alcoholic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterAlcohol extends RecyclerView.Adapter<AdapterAlcohol.Holder>  {

    Context context;
    List<Drink> drinks;

    public AdapterAlcohol(Context context, List<Drink> drinks) {
        this.context = context;
        this.drinks = drinks;
    }
    public AdapterAlcohol(List<Drink> drinks) {
        this.drinks = drinks;

    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.fragment_alcohol, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Glide.with(context).load(drinks.get(position).getIdDrink()).into((ImageView) holder.itemView);
    }


    @Override
    public int getItemCount() {
        return drinks.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView.findViewById(R.id.imageView2);
            textView.findViewById(R.id.strDrink);
            textView.findViewById(R.id.strDrinkThumb);
            textView.findViewById(R.id.idDrink);
        }
    }
}
