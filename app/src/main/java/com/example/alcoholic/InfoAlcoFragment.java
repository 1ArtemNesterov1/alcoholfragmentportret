package com.example.alcoholic;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;


public class InfoAlcoFragment extends Fragment {

    public static final String EXTRA_KEY = "extra_text";
    private Drink drink;
    private TextView textView;
    ImageView imageView;

    public static InfoAlcoFragment newInstance(Drink drink) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_KEY, drink);
        InfoAlcoFragment fragment = new InfoAlcoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        drink = arg.getParcelable(EXTRA_KEY);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.alco_info//расположить на разметке имедж//, container, false);
        textView = root.findViewById(R.id.text);
        textView.setText(drink.getStrDrink());
        //расположить имеджвью
        Glide.with(textView.getContext()).load(drink.getStrDrinkThumb()).into(itemView);
        return root;
    }

}
