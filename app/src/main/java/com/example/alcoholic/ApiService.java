package com.example.alcoholic;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://www.thecocktaildb.com/";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<List<RequestModel>> getFactsByAlcohol(String strDrink, String strDrinkThumb, String idDrink) {
        return privateApi.getFactsByAnimalAlcohol(strDrink, strDrinkThumb, idDrink);
    }


    public interface PrivateApi {
        @GET("api.php")
        Observable<List<RequestModel>> getFactsByAnimalAlcohol(@Query("strDrink") String strDrink, @Query("strDrinkThumb") String strDrinkThumb, @Query("idDrink") String idDrink);
    }
}
