package com.example.alcoholic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Coordinator{

    FrameLayout container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);



        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.rootFragment, new AlcoholFragment())
                .commit();




        container = findViewById(R.id.root);


        if (savedInstanceState == null) {
            //для показа ореинтации первый запуск
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                initPortrait();
            } else {
                initLandscape();
            }


            //новый инстанс для работы
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, new AlcoholFragment())
                    .commit();
        } else {
            //тут уже был для востановления
        }


    }

    //для альбомного
    private void initLandscape() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new AlcoholFragment())
                .commit();


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_right, InfoAlcoFragment.newInstance(""))
                .commit();
    }


    //для портрета
    private void initPortrait() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new AlcoholFragment())
                .commit();
    }


    @Override
    public void showNextFragment(String text) {
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_PORTRAIT) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, InfoAlcoFragment.newInstance(text))
                    .addToBackStack(null)//для возврата на придедущее активити(экран)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_right, InfoAlcoFragment.newInstance(text))
                    .commit();
        }

    }
    }

